# ngpoint

A carousel for HTML content

## `npm install @esradeniz/slider`

## Usage

Import slider in your .module.js and add it to dependencies.

```javascript
import '@esradeniz/slider';

angular.module('app', [slider]);
```

Then, use it in your template as such:
s

```html
<slide-deck>
    <slide>
        <img src="path-to-image" />
    </slide>
    <slide>
        <img src="path-to-image" />
    </slide>
    <slide>
        <img src="path-to-image" />
    </slide>
</slide-deck>
```
