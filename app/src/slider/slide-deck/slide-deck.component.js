import Template from './slide-deck.template.html';

class SlideDeckComponent {
  constructor($interval, $scope)
  {
    this.autoPlayToken =false;
    const slides = [];
    this.slides =slides;
    this.currentSlide = 0;
    this._$interval = $interval;
  }

  select(selectedSlide) {
    Array.prototype.forEach.call(this.slides, (slide) => {
      slide.isSelected = false;
    });
    selectedSlide.isSelected = true;
  };

  addSlide(slide) {
    if (this.slides.length === 0) {
      this.select(slide);
    }
    this.slides.push(slide);
  };

  next() {
    if (this.currentSlide < this.slides.length - 1) {
      this.currentSlide += 1;
    } else {
      this.currentSlide = 0;
    }
    this.select(this.slides[this.currentSlide]);
  };     

  prev() {
    if (this.currentSlide > 0) {
      this.currentSlide -= 1;
    } else {
      this.currentSlide = this.slides.length - 1;
    }
      this.select(this.slides[this.currentSlide]);
  };

  playAuto() {
    if (this.autoPlayToken) {
      this._$interval.cancel(this.autoPlayToken);
      this.autoPlayToken = false;
    } else {
      this.autoPlayToken = this._$interval(this.next = () => {
        if (this.currentSlide < this.slides.length - 1) {
          this.currentSlide += 1;
        } else {
          this.currentSlide = 0;
        }
        this.select(this.slides[this.currentSlide]);
      }, 1000);
    }
  }; 
}

export default {
  controller: ['$interval','$scope', SlideDeckComponent],
  template: Template,
  transclude: true,
}