import Template from './slide.template.html';

class SlideComponent {
  constructor()
  {
    this.$onInit = () => {
      this.parent.addSlide(this);
    }
  }  
}
export default {
  controller : [SlideComponent],
  template : Template,
  transclude: true,
  require: {
    parent: '^slideDeck',
  },
};