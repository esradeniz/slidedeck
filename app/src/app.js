import 'angular';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './app.css';
import './slider/slider.module'

const slider = angular.module('app', ['slider']);
export default slider.name;


